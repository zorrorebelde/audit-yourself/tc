# About `textlint`

is use to keep some quality on texts.

## Installation

If you don't have it already, install `textlint`:

```shell
npm install -g textlint # install the tool globally
```

You also need to install the rules use in this project.

```shell
npm install -g  textlint-rule-write-good textlint-rule-common-misspellings textlint-rule-terminology
```
