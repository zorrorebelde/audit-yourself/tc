package main

import (
	"flag"
	"fmt"
	"os"
)

func app() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s <output path>\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		os.Exit(1)
	}
}
