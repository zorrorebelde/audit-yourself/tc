package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var header = []string{"Name", "Rank", "Symbol", "Homepage", "Categories", "Blockchain Sites", "Source Code Repositories"}

func getFirstOrEmpty(l []string) string {
	if len(l) > 0 {
		return l[0]
	}

	return ""
}

func coinInfoToCSV(ci *coinInfo) []string {
	return []string{
		ci.Name,
		fmt.Sprintf("%d", ci.Rank), // I'm lazy, okay?
		strings.ToUpper(ci.Symbol),
		getFirstOrEmpty(ci.Homepage),
		strings.Join(ci.Categories, ","),
		getFirstOrEmpty(ci.BlockchainSites),
	} // store the rest in markdown files
}

func renderCSV(outputPath string, coins []*coinInfo) error {
	err := os.MkdirAll(outputPath, os.ModePerm)
	if err != nil {
		return err
	}

	file, err := os.Create(filepath.Join(outputPath, "topcoins.csv"))
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	err = writer.Write(header)
	if err != nil {
		return err
	}

	for index, coin := range coins {
		row := coinInfoToCSV(coin)
		err = writer.Write(row)
		if err != nil {
			return fmt.Errorf("failed to write '%s' (index: %d) data into CSV: %v", coin.Name, index, err)
		}
	}

	return nil
}
