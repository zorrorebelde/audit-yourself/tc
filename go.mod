module gitlab.com/zorrorebelde/audit-yourself/tc

go 1.20

require gitlab.com/zorrorebelde/go-coingecko v0.4.4-RC1

require (
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
	github.com/insprac/qe v0.1.2 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
)
