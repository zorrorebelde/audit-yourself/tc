package main

import (
	"flag"
	"os/user"
	"path/filepath"
)

func init() {
	// setup positional arguments
	app()
}

func main() {
	marketsFetcher := NewMarkets()

	err := marketsFetcher.Fetch()
	if err != nil {
		panic(err) // TODO: gracefully fail here
	}

	sortedCoins := marketsFetcher.ListCoinsSorted()

	// get output path
	outputPath := flag.Arg(0)

	{ // replace ~/ with the full home directory
		usr, err := user.Current()
		if err != nil {
			panic(err) // TODO: gracefully fail here
		}

		if outputPath[:2] == "~/" {
			outputPath = filepath.Join(usr.HomeDir, outputPath[2:])
		}
	}

	// write CSV data
	err = renderCSV(outputPath, sortedCoins)
	if err != nil {
		panic(err) // TODO: gracefully fail here
	}

	// write markdown files
	for _, coin := range sortedCoins {
		err = renderMarkdown(filepath.Join(outputPath, "profiles"), coin)
		if err != nil {
			panic(err) // TODO: gracefully fail here
		}
	}
}
