package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

var funcMap = template.FuncMap{
	"upper": strings.ToUpper,
}

const markdownTmpl = `# {{.Name}} ({{upper .Symbol}})

🛈 Data provided by *CoinGecko*, view {{.Name}} complete profile [at CoinGecko](https://coingecko.com/coins/{{.ID}}).

## Homepage

{{if .Homepage}}{{range .Homepage}}- {{.}}
{{end}}{{else}}⚠ No Homepage links were available.
{{end}}
## Blockchain sites

{{if .BlockchainSites}}{{range .BlockchainSites}}- {{.}}
{{end}}{{else}}⚠ No Blockchain sites were available.
{{end}}
## Source code repositories

{{if .CodeRepo}}{{range .CodeRepo}}- {{.}}
{{end}}{{else}}⚠ No source code repository links were available.
{{end}}`

func renderMarkdown(outputPath string, ci *coinInfo) error {
	err := os.MkdirAll(outputPath, os.ModePerm)
	if err != nil {
		return err
	}

	t, err := template.New("coin-profile").Funcs(funcMap).Parse(markdownTmpl)
	if err != nil {
		return err
	}

	file, err := os.Create(filepath.Join(outputPath, fmt.Sprintf("%s-%s.md", ci.Name, ci.Symbol)))
	if err != nil {
		return err
	}
	defer file.Close()

	return t.Execute(file, ci)
}
