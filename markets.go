package main

import (
	"fmt"
	"log"
	"regexp"
	"sort"

	coingecko "gitlab.com/zorrorebelde/go-coingecko/v3"
)

const (
	pages   = 4
	perPage = 250
)

var (
	keyword = regexp.MustCompile(`(?i)[^E]+Ecosystem`)
	EOPErr  = fmt.Errorf("end of paged data")
)

func getMarketsParams(page int, category string) coingecko.GetCoinMarketsParams {
	if category != "" {
		return coingecko.GetCoinMarketsParams{VSCurrency: "usd", Order: "market_cap_desc", PerPage: perPage, Page: uint16(page), Category: category}
	}
	return coingecko.GetCoinMarketsParams{VSCurrency: "usd", Order: "market_cap_desc", PerPage: perPage, Page: uint16(page)}
}

func combine(slices ...[]string) []string {
	result := []string{}
	for _, s := range slices {
		result = append(result, s...)
	}
	return result
}

func removeEmptyStrings(list []string) []string {
	var cleanList []string
	for _, item := range list {
		if item != "" {
			cleanList = append(cleanList, item)
		}
	}

	return cleanList
}

func cleanCoinInfo(coins []*coinInfo) {
	for _, coin := range coins {
		coin.BlockchainSites = removeEmptyStrings(coin.BlockchainSites)
		coin.Categories = removeEmptyStrings(coin.Categories)
		coin.Homepage = removeEmptyStrings(coin.Homepage)
		coin.CodeRepo = removeEmptyStrings(coin.CodeRepo)
	}
}

type coinInfo struct {
	ID              string
	Name            string
	Rank            int
	Symbol          string
	Categories      []string
	Homepage        []string
	CodeRepo        []string
	BlockchainSites []string
}

func NewMarkets() *Markets {
	return &Markets{
		coins:      make(map[string]*coinInfo),
		ecosystems: make(map[string]string),
	}
}

type Markets struct {
	coins      map[string]*coinInfo
	ecosystems map[string]string
}

func (m *Markets) Fetch() error {
	categories, err := coingecko.GetCoinsCategoriesList()
	if err != nil {
		return fmt.Errorf("cannot fetch list of coin categories: %v", err)
	}

	// use the name as key
	for _, category := range categories {
		m.ecosystems[category.Name] = category.ID
	}

	// fetch the top 1000 coins first
	for page := 1; page < pages; page++ {
		err := m.FetchPageEcosystem(page, "")
		if err != nil {
			if err != EOPErr {
				return err
			}
		}
	}

	// now, fetch the coins for each ecosystem found
	// this entire thing may take a while...
	for _, ecosystem := range m.ListEcosystemsFound() {
		var err error = nil
		page := 1
		for err == nil {
			err = m.FetchPageEcosystem(page, ecosystem)
			page++
		}
		if err != nil {
			if err != EOPErr {
				return err
			}
		}
	}
	return nil
}

func (m *Markets) FetchPageEcosystem(page int, ecosystem string) error {
	response, err := coingecko.GetCoinMarkets(getMarketsParams(page, ecosystem))
	if err != nil {
		return fmt.Errorf("request error at page %d: %v", page, err)
	}

	// check if we reached the last page
	if len(response) == 0 {
		return EOPErr
	}

	for coinIndex, coin := range response {
		if ok := m.coins[coin.ID]; ok == nil {
			// fetch coin's profile
			coinProfile, err := coingecko.GetCoin(coin.ID, coingecko.GetCoinParams{
				Localization:  "en",
				Tickers:       false,
				MarketData:    false,
				CommunityData: false,
				DeveloperData: false,
				Sparkline:     false,
			})
			if err != nil {
				return fmt.Errorf("request error at page %d on Markets result %d (%s - %s): %v", page, coinIndex, coin.ID, coin.Name, err)
			}
			// record this coin info
			info := &coinInfo{
				ID:              coin.ID,
				Name:            coin.Name,
				Symbol:          coin.Symbol,
				Rank:            int(coin.MarketCapRank),
				Categories:      coinProfile.Categories,
				Homepage:        coinProfile.Links.Homepage,
				CodeRepo:        combine(coinProfile.Links.ReposURL.GitHub, coinProfile.Links.ReposURL.Bitbucket),
				BlockchainSites: coinProfile.Links.BlockchainSite,
			}

			m.coins[coin.ID] = info
		}
	}

	return nil
}

func (m *Markets) ListEcosystemsFound() []string {
	var list []string
	for _, coin := range m.coins {
		for _, category := range coin.Categories {
			if keyword.MatchString(category) {
				id := m.ecosystems[category]
				if id != "" {
					list = append(list, id)
				} else {
					log.Printf("⚠ Category '%s' has no ID!", category)
				}
			}
		}
	}

	return list
}

func (m *Markets) ListCoinsSorted() []*coinInfo {
	var list []*coinInfo

	for _, coin := range m.coins {
		list = append(list, coin)
	}

	sort.Slice(list, func(i int, j int) bool {
		return list[i].Rank < list[j].Rank
	})

	// clean slices in each struct for each coin
	cleanCoinInfo(list)

	return list
}
